<?php
/**
* Created by PhpStorm.
* User: red_fox
* Date: 21.05.21
* Time: 15:06
*
* Класс работы с кешем
*/

class Cache
{
    private $cachePath = './cache/';

    /**
     * Метод возвращает список файлов, которые есть в кеше
     */
    public function getFileList()
    {
        return array_diff(scandir($this->cachePath), array('..', '.'));
    }

    /**
     * Метод возвращает закешированные данные за определённую дату, если они есть
     * Если данных нет за указанную дату - возвращает false
     */
    public function get($date)
    {
        $file = $this->cachePath . $date . '.json';

        if (file_exists($file)) {
            return json_decode(file_get_contents($file));
        }

        return false;
    }

    /**
     * Метод сохраняет данные в кеше
     */
    public function store($rates)
    {
        $file    = $this->cachePath . $rates['Date'] . '.json';
        $content = json_encode($rates, JSON_UNESCAPED_UNICODE);

        if (file_put_contents($file, $content) === false) {
            return false;
        }

        return json_decode($content);
    }
}
