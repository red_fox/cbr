<?php
/**
 * Created by PhpStorm.
 * User: red_fox
 * Date: 21.05.21
 * Time: 20:28
 *
 * Класс отрисовки фронта
 */

class Draw
{
    /**
     * Метод отрисовки формы ввода дат
     */
    public static function form($rates)
    {
        $result = '';

        $cache = new Cache();

        $result .= '<form method="post">';

        $result .= '<fieldset>';
        $result .= '<legend>Дата</legend>';
        $result .= '<select name="date_select">';
        $result .= '<option value="">Выбрать дату</option>';
        foreach ($cache->getFileList() as $file) {
            $date = str_replace('.json', '', $file);
            $result .= '<option value="' . $date . '">' . $date . '</option>';
        }
        $result .= '</select>';
        $result .= '<input name="date" type="text" placeholder="Указать другую дату (dd.mm.yyyy)">';
        $result .= '</fieldset>';

        $result .= '<fieldset>';
        $result .= '<legend>Валюта для выбора кросс-курса</legend>';
        $result .= '<label> Код валюты: ';
        $result .= '<select name="currency_code">';
        $result .= '<option value="">Выбрать валюту</option>';
        $result .= '<option value="RUR">RUR</option>';
        foreach ($rates->Valute as $currencyItem) {
            $result .= '<option value="' . $currencyItem->CharCode . '">' . $currencyItem->CharCode . '</option>';
        }
        $result .= '</select>';
        $result .= '</label>';
        $result .= '<br/>';
        $result .= '<label> Код базовой валюты: ';
        $result .= '<select name="currency_base_code">';
        $result .= '<option value="RUR">RUR</option>';
        foreach ($rates->Valute as $currencyItem) {
            $result .= '<option value="' . $currencyItem->CharCode . '">' . $currencyItem->CharCode . '</option>';
        }
        $result .= '</select>';
        $result .= '</label>';
        $result .= '</fieldset>';

        $result .= '<input type="submit" value="Выбрать">';

        $result .= '</form>';

        return $result;
    }

    /**
     * Метод отрисовки таблицы курсов
     */
    public static function table($rates)
    {
        $result = '';

        $result .= '<h1>Курс на ' . $rates->{'@attributes'}->Date . '</h1>';

        $result .= '<table>';
        foreach ($rates->Valute as $currencyItem) {
            $result .= '<tr>';
            $result .= '<td>' . $currencyItem->Name . '</td>';
            $result .= '<td>' . $currencyItem->CharCode . '</td>';
            $result .= '<td>' . $currencyItem->Value . '</td>';
            $result .= '</tr>';
        }
        $result .= '</table>';

        return $result;
    }

    /**
     * Метод отрисовки кросс-курсов
     */
    public static function crossRate($date, $crossRate)
    {
        $result = '';

        $result .= '<h1>Кросс-курс ' . $crossRate['selected']['currency']->Name
            . '/' . $crossRate['selected']['baseCurrency']->Name
            . ' на ' . (empty($date)?'сегодня':$date) . '</h1>';

        $result .= 'Курс: ' . number_format($crossRate['selected']['crossRate'], 2, ',', '');

        $crossRateDifference = number_format($crossRate['selected']['crossRate']-$crossRate['previous']['crossRate'], 2, ',', '');
        $result .= '<br/>';
        $result .= 'Разница с предыдущим днём: ' . $crossRateDifference;

        return $result;
    }
}
