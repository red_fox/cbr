<?php
/**
* Created by PhpStorm.
* User: red_fox
* Date: 21.05.21
* Time: 14:45
*
* Класс работы с курсами
*/

class Rates
{
    /**
     * Метод получения курсов за указанную дату.
     * Если дата пустая, то вернёт спсок курсов за текущую дату.
     * Ищет вначале в кеше (а в друг есть), если нет делает запрос к API ЦБР
     *
     * Формат даты 'd.m.Y' - мало ли, будем хранить в дальнейшем в БД
     *
     * Если в кеше нет и API недоступно - вернёт false
     */
    public function get($date = '')
    {
        if ($date == '') {
            $date = date('d.m.Y');
        }

        $cache = new Cache();

        $rates = $cache->get($date);

        return ($rates !== false)?$rates:$this->getCbr($date);
    }

    /**
     * Метод получения курсов с сайта ЦБР
     * Кладёт полученные данные в кеш
     * Возвращает массив курсов
     */
    private function getCbr($date)
    {
        // Получаем данные из API ЦБР
        $ratesXml = $this->requestCbr($date);

        if ($ratesXml !== false) {

            // Распарсим XML в объект
            $rates = simplexml_load_string($ratesXml);

            // Кладём данные к кеш
            $cache = new Cache();
            return $cache->store($rates);
        }

        return false;
    }

    /**
     * Метод запроса к API ЦБР
     */
    private function requestCbr($date)
    {
        // Преобразуем дату в формат, который принимает API ЦБР
        $date = date('d/m/Y', strtotime($date));

        $cbrApi = 'http://cbr.ru/scripts/XML_daily.asp';

        $url = $cbrApi . '?date_req=' . $date;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpCode != 200) {
            return false;
        }

        return $result;
    }

    /**
     * Метод получения кросс-курса указанной валюты за определённый день
     */
    public function getCrossRate($date, $currency, $baseCurrency)
    {
        // Получаем курсы за указанный день
        $ratesBySelectedDay = $this->get($date);
        // Получаем курсы за предыдущий день
        $ratesByPreviousDay = $this->get(date('d.m.Y', strtotime($date . ' -1 day')));

        $selected = $this->getCrossRateCurrency($ratesBySelectedDay, $currency, $baseCurrency);
        $previous = $this->getCrossRateCurrency($ratesByPreviousDay, $currency, $baseCurrency);

        return [
            'selected' => [
                'currency'     => $selected['currency'],
                'baseCurrency' => $selected['baseCurrency'],
                'crossRate'    => $selected['currency']->Value/$selected['baseCurrency']->Value,
            ],
            'previous' => [
                'currency'     => $previous['currency'],
                'baseCurrency' => $previous['baseCurrency'],
                'crossRate'    => $previous['currency']->Value/$previous['baseCurrency']->Value,
            ]
        ];
    }

    /**
     * Метод получения двух валют, которые будем соотносить из общего списка валют
     */
    private function getCrossRateCurrency($rates, $currency, $baseCurrency)
    {
        $result = [
            'currency'     => '',
            'baseCurrency' => '',
        ];

        // Выбираем две нужные нам валюты среди курсов за указанный день
        foreach ($rates->Valute as $currencyItem) {
            // Валюта, которую будем соотносить
            if ($currencyItem->CharCode == $currency) {
                // Приведём числа в нормальный вид
                $currencyItem->Value = str_replace(',', '.', $currencyItem->Value);
                $result['currency'] = $currencyItem;
            }
            // Валюта, к которой будем соотносить
            if ($currencyItem->CharCode == $baseCurrency) {
                // Приведём числа в нормальный вид
                $currencyItem->Value = str_replace(',', '.', $currencyItem->Value);
                $result['baseCurrency'] = $currencyItem;
            }
        }

        // Если совпадений по коду валюты не было, значит скорее всего валюта - Российский рубль
        if (empty($result['baseCurrency'])) {
            $result['baseCurrency'] = $this->currencyRur();
        }
        if (empty($result['currency'])) {
            $result['currency'] = $this->currencyRur();
        }

        return $result;
    }

    /**
     * Метод возвращающий объект валюты Российского рубля
     */
    private function currencyRur()
    {
        $currency        = new stdClass();
        $currency->Name  = 'Российский рубль';
        $currency->Value = 1;

        return $currency;
    }
}
