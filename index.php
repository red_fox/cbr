<?php
/**
* Created by PhpStorm.
* User: red_fox
* Date: 21.05.21
* Time: 14:31
*
*/

require_once('./library/cache.php');
require_once('./library/rates.php');
require_once('./library/draw.php');

$rates = new Rates();

$date = '';

// Если дата указана вручную через input
if (!empty($_POST['date'])) {

    // Проверяем на корректрость ввода
    $date = date('d.m.Y', strtotime($_POST['date']));
}

// Если дата выбрана из select`а
if (!empty($_POST['date_select'])) {
    $date = $_POST['date_select'];
}

// Если выбрана валюта, значит будем смотреть кросс-курс
if (!empty($_POST['currency_code'])) {

    $crossRate = $rates->getCrossRate($date, $_POST['currency_code'], $_POST['currency_base_code']);

    echo Draw::crossRate($date, $crossRate);

} else {

    // Получим курсы за определённую дату
    $currentRates = $rates->get($date);

    if ($currentRates !== false) {

        // Отрисуем форму с выпадайкой и вводом даты
        echo Draw::form($currentRates);

        // Отрисуем таблицу курсов
        echo Draw::table($currentRates);
    } else {
        echo 'Что-то пошло совсем не так';
    }
}
